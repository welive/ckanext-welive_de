import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
from ckan.logic.action import get
from ckan.logic import NotFound, NotAuthorized
import requests
import ConfigParser
import os
import json
import logging
import ckan.model as model
from ckan.common import request
from urlparse import urlparse
import redis

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

PLUGIN_SECTION = 'plugin:welive_utils'
WELIVE_API = config.get(PLUGIN_SECTION, 'welive_api')

BASIC_USER = config.get(PLUGIN_SECTION, 'basic_user')
BASIC_PASSWORD = config.get(PLUGIN_SECTION, 'basic_password')


# PILOT_DICT = {'bilbao': 'Bilbao', 'novi-sad': 'Novisad',
#               'helsinki-uusimaa': 'Uusimaa', 'trento': 'Trento'}
PILOT_DICT = json.loads(config.get(PLUGIN_SECTION, 'pilot_dict'))

REDIS_SECTION = 'redis'
REDIS_HOST = config.get(REDIS_SECTION, 'redis.host')
REDIS_PORT = config.get(REDIS_SECTION, 'redis.port')
REDIS_DATABASE = config.get(REDIS_SECTION, 'redis.mkt.database')

log = logging.getLogger(__name__)

r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DATABASE)


def getJSONData(pkg_dict):
    tag_list = []
    for tag_dict in pkg_dict["tags"]:
        tag_list.append(tag_dict["name"])
    lang = None
    for extra in pkg_dict['extras']:
        if extra["key"] == 'language':
            lang = extra['value']
    if lang is not None:
        data = {'lang': lang,
                'tags': tag_list
                }
        return json.dumps(data)
    return None


class Welive_DePlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IPackageController, inherit=True)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'welive_de')

    # IPackageController

    def after_create(self, context, pkg_dict):
        return pkg_dict

    def after_update(self, context, pkg_dict):
        return pkg_dict

    def after_delete(self, context, pkg_dict):
        return pkg_dict

    def after_show(self, context, pkg_dict):
        return pkg_dict

    def before_search(self, search_params):
        return search_params

    def after_search(self, search_results, search_params):
        return search_results

    def before_index(self, pkg_dict):
        return pkg_dict

    def before_view(self, pkg_dict):
        try:
            if '/dataset/' in urlparse(request.url).path and PILOT_DICT.get(pkg_dict['organization']['name'], None) is not None:
                if not r.exists(pkg_dict.get('name', '')):
                    response = requests.get(
                        '{}/mkp/get-all-artefacts/pilot-id/{}/'
                        'artefact-types/Dataset'.format(
                            WELIVE_API, PILOT_DICT[pkg_dict['organization']['name']]),
                        auth=(BASIC_USER, BASIC_PASSWORD)
                    )
                    source_id = None
                    mkt_datasets = response.json()['artefacts']
                    for item in mkt_datasets:
                        r.set(item['eId'], item['artefactId'])
                        if item['eId'] == pkg_dict['name']:
                            source_id = item['artefactId']
                            break
                else:
                    source_id = r.get(pkg_dict.get('name', None))

                if source_id is not None:
                    response = requests.get(
                        '{}/de/dataset/{}/recommend/datasets'.format(WELIVE_API,
                                                                     source_id),
                        auth=(BASIC_USER, BASIC_PASSWORD)
                    )
                    if response.status_code == 200:
                        rec_datasets = response.json()
                        target_dataset_ids = []
                        log.debug(rec_datasets)
                        for dataset_id in rec_datasets:
                            log.debug('{}/mkp/get-artefact/artefactid/{}'.format(
                                WELIVE_API, dataset_id))
                            response = requests.get(
                                '{}/mkp/get-artefact/artefactid/{}'.format(
                                    WELIVE_API, dataset_id),
                                auth=(BASIC_USER, BASIC_PASSWORD))
                            response_json = response.json()
                            log.debug(response_json)
                            if len(response_json) > 0:
                                if response_json['eId'] != pkg_dict['name']:
                                    target_dataset_ids.append(response_json['eId'])
                            if len(target_dataset_ids) >= 10:
                                break

                        target_datasets = []
                        for _id in target_dataset_ids:
                            try:
                                package = get.package_show({'model': model}, {'id': _id})
                                target_datasets.append(package)
                            except NotFound:
                                log.error('Dataset {} not found!'.format(_id))
                            except NotAuthorized as e:
                                log.error(e)
                        pkg_dict['recommended_datasets'] = target_datasets
                    else:
                        pkg_dict['recommended_datasets'] = []
        except Exception as e:
            log.error('Can not get recommendations')
            log.error(e)

        return pkg_dict
